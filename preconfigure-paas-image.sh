#!/bin/bash
set -e
export ANSIBLE_INVENTORY=${ANSIBLE_INVENTORY:-/inventory/${OPENSHIFT_CLUSTER}}
# NB: when running on a VM with 4GB of memory, reduce default concurrency to avoid running out of memory (openshift-ansible default is 20)
export ANSIBLE_FORKS=${ANSIBLE_FORKS:-10}

sed -i "${ANSIBLE_CONFIG}" -e 's/^forks\s*=.*$/forks = '${ANSIBLE_FORKS}'/'

# start logstash as a background process to openshift-ansible logs to elasticsearch
echo "starting logstash..."
/usr/share/logstash/bin/logstash -f /paas-tools/config/logstash.conf > ~/logstash-startup.log 2>&1  &

# We observed that some docker environments time out when retrieving localhost facts.
# This results in errors when executing playbooks.
# Detect such a bad environment early before running any playbook by trying to load localhost facts.
# If this fails with a timeout error, it will be required to run the paas-tools image from another machine.
echo -n "Checking that localhost facts work... "
ansible localhost -m setup -i /dev/null >/tmp/ansible_localhost_facts_output 2>&1 || (cat /tmp/ansible_localhost_facts_output; exit 1)
echo "OK"

if [[ $# -lt 1 ]]; then
  # no arguments passed to entrypoint, run a shell
  exec /bin/bash
fi

# arguments passed to the entrypoint are considered a command to be run
exec "$@"
