FROM gitlab-registry.cern.ch/ai-config-team/ai-tools:latest

# actual version to use is set in .gitlab-ci.yml!
ARG ANSIBLE_VERSION=2.6.8

....

...

# Install logstash to send openshift-ansible logs to elasticsearch
RUN rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
RUN echo $'[logstash-7.x] \n\
name=Elastic repository for 7.x packages \n\
baseurl=https://artifacts.elastic.co/packages/7.x/yum \n\
gpgcheck=1 \n\
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch \n\
enabled=1 \n\
autorefresh=1 \n\
type=rpm-md' \n\
> /etc/yum.repos.d/logstash.repo
RUN yum install logstash -y

# Pre-configure the paas-tool image as wanted in preconfigure-paas-image.sh
ENTRYPOINT ["/paas-tools/preconfigure-paas-image.sh"]
